<?php namespace Redtally\CurrencyFieldType\Validation;

use Redtally\CurrencyFieldType\CurrencyFieldType;

/**
 * Class ValidateCurrency
 * @package Redtally\CurrencyFieldType\Validation
 */
class ValidateCurrency
{

    /**
     * Handle the validation.
     *
     * @param CurrencyFieldType $fieldType
     * @param                  $value
     * @return bool
     */
    public function handle(CurrencyFieldType $fieldType, $value)
    {
        return in_array(strtoupper($value), array_keys($fieldType->getOptions()));
    }
}
