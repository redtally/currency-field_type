<?php namespace Redtally\CurrencyFieldType;

use Anomaly\Streams\Platform\Addon\FieldType\FieldType;
use Redtally\CurrencyFieldType\Command\BuildOptions;
use Redtally\CurrencyFieldType\Validation\ValidateCurrency;

/**
 * Class CurrencyFieldType
 * @package Redtally\CurrencyFieldType
 */
class CurrencyFieldType extends FieldType
{
    /**
     * The filter view.
     *
     * @var string
     */
    protected $filterView = 'redtally.field_type.currency::filter';

    /**
     * The default config.
     *
     * @var array
     */
    protected $config = [
        'handler' => 'Redtally\CurrencyFieldType\Handler\DefaultHandler@handle',
    ];

    /**
     * The validation rules.
     *
     * @var array
     */
    protected $rules = [
        'valid_currency',
    ];

    /**
     * The custom validators.
     *
     * @var array
     */
    protected $validators = [
        'valid_currency' => [
            'handler' => ValidateCurrency::class,
            'message' => 'redtally.field_type.currency::message.invalid_currency',
        ],
    ];

    /**
     * The dropdown options.
     *
     * @var null|array
     */
    protected $options = null;

    /**
     * Get the countries.
     *
     * @return array
     */
    public function getOptions()
    {
        if ($this->options === null) {
            $this->dispatch(new BuildOptions($this));
        }

        $topOptions = array_get($this->getConfig(), 'top_options');

        if (!is_array($topOptions)) {
            $topOptions = array_filter(array_reverse(explode("\r\n", $topOptions)));
        }

        foreach ($topOptions as $iso) {
            if (isset($this->options[$iso])) {
                $this->options = [$iso => $this->options[$iso]] + $this->options;
            }
        }

        return array_unique($this->options);
    }

    /**
     * Set the options.
     *
     * @param  array $options
     * @return $this
     */
    public function setOptions(array $options)
    {
        $this->options = $options;

        return $this;
    }

    /**
     * Get the placeholder.
     *
     * @return null|string
     */
    public function getPlaceholder()
    {
        if (!$this->placeholder && !$this->isRequired() && $this->config('mode') == 'dropdown') {
            return 'redtally.field_type.currency::input.placeholder';
        }

        return $this->placeholder;
    }

    /**
     * Return the input view.
     *
     * @return string
     */
    public function getInputView()
    {
        return 'redtally.field_type.currency::' . $this->config('mode', 'input');
    }

    /**
     * Get the class.
     *
     * @return null|string
     */
    public function getClass()
    {
        if ($class = parent::getClass()) {
            return $class;
        }

        return $this->config('mode') == 'dropdown' ? 'custom-select form-control' : 'form-control';
    }
}
