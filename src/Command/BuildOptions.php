<?php namespace Redtally\CurrencyFieldType\Command;

use Illuminate\Container\Container;
use Redtally\CurrencyFieldType\CurrencyFieldType;


/**
 * Class BuildOptions
 * @package Redtally\CurrencyFieldType\Command
 */
class BuildOptions
{

    /**
     * The field type instance.
     *
     * @var CurrencyFieldType
     */
    protected $fieldType;

    /**
     * Create a new BuildOptions instance.
     *
     * @param CurrencyFieldType $fieldType
     */
    function __construct(CurrencyFieldType $fieldType)
    {
        $this->fieldType = $fieldType;
    }

    /**
     * Handle the command.
     *
     * @param Container $container
     */
    public function handle(Container $container)
    {
        $handler = array_get($this->fieldType->getConfig(), 'handler');

        if (is_string($handler) && !str_contains($handler, '@')) {
            $handler .= '@handle';
        }

        $container->call($handler, ['fieldType' => $this->fieldType]);
    }
}
