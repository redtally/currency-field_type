<?php namespace Redtally\CurrencyFieldType\Handler;

use Illuminate\Contracts\Config\Repository;
use Redtally\CurrencyFieldType\CurrencyFieldType;

/**
 * Class DefaultHandler
 * @package Redtally\CurrencyFieldType\Handler
 */
class DefaultHandler
{

    /**
     * Handle the options.
     *
     * @param CurrencyFieldType $fieldType
     * @param Repository       $config
     */
    public function handle(CurrencyFieldType $fieldType, Repository $config)
    {
        $currencies = $config->get('redtally.field_type.currency::codes');

        $names = array_map(
            function ($iso) {
                return 'redtally.field_type.currency::names.' . $iso;
            },
            $currencies
        );

        $options = array_combine($currencies, $names);

        asort($options);

        $fieldType->setOptions($options);
    }
}
