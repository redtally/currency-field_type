<?php namespace Redtally\CurrencyFieldType;

use Anomaly\Streams\Platform\Addon\FieldType\FieldTypeModifier;

/**
 * Class CurrencyFieldTypeModifier
 * @package Redtally\CurrencyFieldType
 */
class CurrencyFieldTypeModifier extends FieldTypeModifier
{

    /**
     * Modify the value.
     *
     * @param $value
     * @return string
     */
    public function modify($value)
    {
        return strtoupper($value);
    }

    /**
     * Restore the value.
     *
     * @param $value
     * @return string
     */
    public function restore($value)
    {
        return strtoupper($value);
    }
}
