<?php namespace Redtally\CurrencyFieldType;

use Anomaly\Streams\Platform\Addon\FieldType\FieldTypePresenter;

/**
 * Class CurrencyFieldTypePresenter
 * @package Redtally\CurrencyFieldType
 */
class CurrencyFieldTypePresenter extends FieldTypePresenter
{

    /**
     * The decorated object.
     * This is for IDE support.
     *
     * @var CurrencyFieldType
     */
    protected $object;

    /**
     * Get the currency code.
     *
     * @return null|string
     */
    public function code()
    {
        if (!$key = $this->object->getValue()) {
            return null;
        }

        return strtoupper($key);
    }

    /**
     * Return the translated currency name.
     *
     * @param  null $locale
     * @return null|string
     */
    public function name($locale = null)
    {
        if (!$key = $this->code()) {
            return null;
        }

        return trans('redtally.field_type.currency::names.' . $key, [], $locale);
    }

    /**
     * Return the currency's symbol.
     *
     * @return null|string
     */
    public function symbol()
    {
        if (!$key = $this->code()) {
            return null;
        }

        if (!$symbol = config('redtally.field_type.currency::symbols.'.$key)) {
            return null;
        }

        return html_entity_decode($symbol);
    }
}
