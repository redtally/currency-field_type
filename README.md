# Currency Field Type
This field type is a select based field type for ISO 4217 currency codes and HTML entities.

### Installation
`composer require redtally/currency-field_type`

`php artisan addon:install redtally.field_type.currency`

### Usage
See the configuration details for the select field type here:
https://pyrocms.com/documentation/select-field-type/latest

### Values
`echo $aStream->currency_field; // outputs 'AUD'`

### Presenters
`echo $aStream->decorated->currency_field->code();` outputs 'AUD'`

`echo $aStream->decorated->currency_field->name();` outputs 'Australia'`

`echo $aStream->decorated->currency_field->symbol();` outputs '&#36;'