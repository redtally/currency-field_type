$(document).on('ajaxComplete ready', function () {

    // Initialize tag inputs.
    $('select[data-provides="redtally.field_type.currency"]:not([data-initialized])').each(function () {

        $(this)
            .attr('data-initialized', '')
            .select2({
                containerCssClass: 'form-control custom-select select2-override',
                dropdownCssClass: 'select2-option-override'
            });
    });
});
