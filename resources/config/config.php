<?php

return [
    'mode'          => [
        'required' => true,
        'type'     => 'anomaly.field_type.select',
        'config'   => [
            'default_value' => 'input',
            'options'       => [
                'input'    => 'redtally.field_type.currency::config.mode.option.input',
                'dropdown' => 'redtally.field_type.currency::config.mode.option.dropdown',
                'search'   => 'redtally.field_type.currency::config.mode.option.search',
            ],
        ],
    ],
    'top_options'   => [
        'type' => 'anomaly.field_type.textarea',
    ],
    'default_value' => [
        'type' => 'redtally.field_type.currency',
    ],
];
