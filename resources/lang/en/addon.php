<?php

return [
    'title'       => 'Currency',
    'name'        => 'Currency Field Type',
    'description' => 'A select based field type for ISO 4217 currencies.'
];
