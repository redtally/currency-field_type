<?php

return [
    'mode'          => [
        'label'        => 'Mode',
        'instructions' => 'What kind of input would you like to display?',
        'option'       => [
            'input'    => 'Text Input',
            'dropdown' => 'Dropdown',
            'search'   => 'Search',
        ],
    ],
    'top_options'   => [
        'name'         => 'Top Options',
        'instructions' => 'Enter the ISO 4217 codes for currencies that should be moved to the top. Enter each code on a new line.',
        'placeholder'  => "USD\nCAD\nAUD",
    ],
    'default_value' => [
        'name'         => 'Default Value',
        'instructions' => 'Select a default currency if any.',
    ],
];
