<?php

return [
    'invalid_currency' => 'The :attribute must be a valid currency code.',
];
